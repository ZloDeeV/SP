# Requirements

# Utils
gulp      = require 'gulp'
gutil     = require 'gulp-util'
rename    = require 'gulp-rename'
fs        = require 'fs'
sh        = require 'shelljs'
path      = require 'path'
concat    = require 'gulp-concat'
plumber   = require 'gulp-plumber'

# Sass
sourcemaps  = require 'gulp-sourcemaps'
sass        = require 'gulp-sass'
cleanCss   = require 'gulp-clean-css'
prefix      = require 'gulp-autoprefixer'

# Pug
pug  = require 'gulp-pug'

# Coffee
coffee = require 'gulp-coffee'
coffeelint = require 'gulp-coffeelint'

# Images
imagemin = require 'gulp-imagemin'

# Paths to source
paths =
  pug: ['./app/views/*.pug']
  pugWatch: ['./app/views/**/*.pug']
  sass: ['./app/assets/stylesheets/app.sass']
  sassWatch: ['./app/assets/stylesheets/**/*.sass']
  coffee: ['./app/assets/coffeescripts/**/*.coffee']
  images: ['./app/assets/images/**/*']
  svg: ['./app/assets/svg/**/*']
  copy: ['./app/assets/fonts']

buildPaths =
  pug: './build/app/'
  sass: './build/app/assets/stylesheets/'
  coffee: './build/app/assets/javascripts/'
  images: './build/app/assets/images/'
  svg: './build/app/assets/images/'

# Gulp compilation tasks
gulp.task 'sass', (done) ->
  gulp.src paths.sass
    .pipe plumber()
    .pipe sass()
    .on 'error', sass.logError
    .pipe prefix 'last 2 versions', 'ios_saf >= 7'
    .pipe cleanCss { keepSpecialComments: 0 }
    .pipe gulp.dest buildPaths.sass

gulp.task 'pug', (done) ->
  gulp.src paths.pug
    .pipe plumber()
    .pipe pug()
    .pipe gulp.dest buildPaths.pug

gulp.task 'coffee', (done) ->
  gulp.src paths.coffee
    .pipe plumber()
    .pipe coffee { bare: true }
    .pipe concat 'app.js'
    .pipe gulp.dest buildPaths.coffee

gulp.task 'images', (done) ->
  gulp.src paths.images
    .pipe gulp.dest buildPaths.images

gulp.task 'images:optimize', (done) ->
  gulp.src paths.images
    .pipe plumber()
    .pipe imagemin
      verbose: true
    .pipe gulp.dest buildPaths.images

gulp.task 'copy', (done) ->
  gulp.src paths.copy
    .pipe gulp.dest './build/app/'

gulp.task 'lint:coffee', (done) ->
  gulp.src paths.coffee
    .pipe coffeelint './coffeelint.json'
    .pipe coffeelint.reporter()

gulp.task 'watch', () ->
  gulp.watch paths.sassWatch, ['sass']
  gulp.watch paths.pugWatch, ['pug']
  gulp.watch paths.coffee, ['coffee']
  gulp.watch paths.data, ['copy']

gulp.task 'clean', () ->
  sh.exec('rm -r build')

gulp.task 'default', ['compile', 'watch']
gulp.task 'compile', ['clean', 'sass', 'pug', 'coffee', 'images', 'copy']
