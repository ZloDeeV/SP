# Snippets for HTML-markup pack

```
Project Root/
│
├── app/
│   └── assets/
│       ├── stylesheets/
│       │   ├── vendor/
│       │   │
│       │   ├── components/
│       │   │
│       │   └── app.sass
│       │
│       ├── images/
│       │
│       └── coffescripts/
│
├── build/
│
├── node_modules/
│
├── .gitignore
├── package.json
├── yarn.lock
└── README.md
```
